prebuilt 64bit amd .deb packages of cdp composers desktop

## Usage

    $ git clone https://coderofsalvation@bitbucket.org/coderofsalvation/cdp-composer-desktop-debian-package.git
    $ cd cdp-composer-desktop-debian-package
    $ dpkg -i cdp-libaaio-0.3.1-1_amd64.deb
    $ dpkg -i cdp-7-1_amd64.dev

> more info: http://www.renoise.com/node/807
